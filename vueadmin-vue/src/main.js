import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

// 引入自定义的全局 css
import '@/assets/css/all.css'
// 引入element-ui
import Element from 'element-ui'
import "element-ui/lib/theme-chalk/index.css"
// 引入axios
import axios from "./axios";

import gobal from "./globalFun"

import "./mock"

Vue.use(Element)

Vue.prototype.$axios = axios

Vue.config.productionTip = false


new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
