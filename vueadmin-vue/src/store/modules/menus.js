import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default {
  state: {
    // 菜单栏数据
    menuList: [],
    // 权限数据
    permList: [],
    hasRoutes: false,
    
    editableTabsValue: 'Index',
    editableTabs: [{
      title: '首页',
      name: 'Index',
    }]
  },
  mutations: {
    setMenuList(state, menus) {
      state.menuList = menus
    },
    setPermList(state, perms) {
      state.permList = perms
    },
    changeRouteStatus(state, hasRoutes) {
      state.hasRoutes = hasRoutes
    },
    
    addTab(state, tab) {
      
      // 解决登陆后登陆页面也被加入到标签页
      if (tab.name === "Login") {
        return
      }
      
      let index = state.editableTabs.findIndex(e => e.name === tab.name)
      
      if (index === -1) {
        state.editableTabs.push({
          title: tab.title,
          name: tab.name,
        });
      }
      
      state.editableTabsValue = tab.name;
    },
    
    resetState: (state) => {
      // console.log("before resetState")
      // console.log(state.menuList)
      // console.log(state.permList)
      // console.log(state.hasRoutes)
      // console.log(state.editableTabsValue)
      // console.log(state.editableTabs)
      state.menuList = []
      state.permList = []
      
      state.hasRoutes = false
      state.editableTabsValue = 'Index'
      state.editableTabs = [{
        title: '首页',
        name: 'Index',
      }]
  
      // console.log("after resetState")
      // console.log(state.menuList)
      // console.log(state.permList)
      // console.log(state.hasRoutes)
      // console.log(state.editableTabsValue)
      // console.log(state.editableTabs)
    }
    
  },
  actions: {},
  
}