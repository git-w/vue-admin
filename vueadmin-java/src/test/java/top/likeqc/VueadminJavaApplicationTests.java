package top.likeqc;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootTest
class VueadminJavaApplicationTests {
    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;
    
    
    @Test
    void contextLoads() {
        String password = bCryptPasswordEncoder.encode("111111");
        System.out.println(password);
    }
    
}
