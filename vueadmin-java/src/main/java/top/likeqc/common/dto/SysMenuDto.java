package top.likeqc.common.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *   {
 *      name: 'SysMenu',
 *      title: '菜单管理',
 *      icon: 'el-icon-menu',
 *      path: '/sys/menus',
 *      component: 'sys/Menu',
 *      children: []
 *   }
 */
@Data
public class SysMenuDto implements Serializable {

    private Long id;

    private String name;
    private String title;
    private String icon;
    private String path;
    private String component;
    private List<SysMenuDto> children = new ArrayList<>();
}
