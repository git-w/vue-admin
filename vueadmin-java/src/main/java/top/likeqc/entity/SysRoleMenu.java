package top.likeqc.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author likeqc
 * @since 2022-03-14
 */
@Data
public class SysRoleMenu {

    private static final long serialVersionUID = 1L;

    private Long roleId;

    private Long menuId;
}
