package top.likeqc.entity;

import lombok.Data;

/**
 * @author likeqc
 * @since 2022-03-14
 */
@Data
public class SysUserRole extends BaseEntity {

    private static final long serialVersionUID = 1L;

    private Long userId;

    private Long roleId;
}
