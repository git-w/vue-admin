package top.likeqc.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import top.likeqc.entity.SysUser;
import top.likeqc.service.SysUserService;

import java.util.List;

@Component
public class UserDetailServiceImpl implements UserDetailsService {

    @Autowired SysUserService sysUserService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        
        SysUser sysUser = sysUserService.getByUsername(username);
        if (sysUser == null) {
            throw new UsernameNotFoundException("用户名或密码不正确");
        }

        return new AccountUser(sysUser.getId(), sysUser.getUsername(), sysUser.getPassword(), getUserAuthority(sysUser.getId()));
    }
    
    /**
     * 获取用户权限信息 （角色、菜单 权限）
     * @param userId
     * @return
     */
    public List<GrantedAuthority> getUserAuthority(Long userId) {
        // 角色(ROLE_admin)、菜单 权限(sys:user:list)
        String authority = sysUserService.getUserAuthorityInfo(userId); // ROLE_admin,sys:user:list,...
        
        
        return AuthorityUtils.commaSeparatedStringToAuthorityList(authority);
    }
}
