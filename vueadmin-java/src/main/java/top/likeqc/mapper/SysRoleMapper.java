package top.likeqc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import top.likeqc.entity.SysRole;

/**
 * Mapper 接口
 *
 * @author likeqc
 * @since 2022-03-14
 */
public interface SysRoleMapper extends BaseMapper<SysRole> {}
