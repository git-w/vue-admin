package top.likeqc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import top.likeqc.entity.SysRoleMenu;

/**
 * Mapper 接口
 *
 * @author likeqc
 * @since 2022-03-14
 */
public interface SysRoleMenuMapper extends BaseMapper<SysRoleMenu> {}
