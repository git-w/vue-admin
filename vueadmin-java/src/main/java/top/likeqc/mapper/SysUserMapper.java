package top.likeqc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import top.likeqc.entity.SysUser;

import java.util.List;

/**
 * Mapper 接口
 *
 * @author likeqc
 * @since 2022-03-14
 */
// @Mapper
// @Service
@Repository
public interface SysUserMapper extends BaseMapper<SysUser> {
    List<Long> getNavMenuId(Long userId);
    
    List<SysUser> listByMenuId(Long menuId);
}
