package top.likeqc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import top.likeqc.common.lang.Result;
import top.likeqc.service.SysUserService;

@RestController
public class TestController {
    @Autowired SysUserService userService;

    @Autowired BCryptPasswordEncoder bCryptPasswordEncoder;

    @PreAuthorize("hasRole('admin')")
    @GetMapping("/test")
    public Object test() {
        return Result.success(userService.list());
    }
    
    @PreAuthorize("hasRole('sys:userr:list')")
    @GetMapping("/test/pass")
    public Object pass() {
        
        // 加密后的密码
        String password = bCryptPasswordEncoder.encode("111111");
    
        boolean matches = bCryptPasswordEncoder.matches("111111", password);

        System.out.println("匹配结果：" + matches);
        
        return Result.success(password);
    }
    
}
