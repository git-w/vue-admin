package top.likeqc.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 前端控制器
 *
 * @author likeqc
 * @since 2022-03-14
 */
@RestController
@RequestMapping("/sys-user-role")
public class SysUserRoleController extends BaseController {}
