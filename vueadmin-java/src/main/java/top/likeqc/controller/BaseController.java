package top.likeqc.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.ServletRequestUtils;
import top.likeqc.service.*;
import top.likeqc.util.RedisUtil;

import javax.servlet.http.HttpServletRequest;

public class BaseController {
    @Autowired HttpServletRequest req;

    @Autowired RedisUtil redisUtil;

    @Autowired SysUserService sysUserService;

    @Autowired SysRoleService sysRoleService;

    @Autowired SysMenuService sysMenuService;

    @Autowired SysUserRoleService sysUserRoleService;

    @Autowired SysRoleMenuService sysRoleMenuService;
    
    /**
     * 获取页码
     *
     * @return Page
     */
    public Page getPage() {
        int current = ServletRequestUtils.getIntParameter(req, "current", 1);
        int size = ServletRequestUtils.getIntParameter(req, "size", 10);

        return new Page(current, size);
    }
}
