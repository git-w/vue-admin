package top.likeqc.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import top.likeqc.common.dto.PassDto;
import top.likeqc.common.lang.Const;
import top.likeqc.common.lang.Result;
import top.likeqc.entity.SysRole;
import top.likeqc.entity.SysUser;
import top.likeqc.entity.SysUserRole;

import java.security.Principal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 前端控制器
 *
 * @author likeqc
 * @since 2022-03-14
 */
@RestController
@RequestMapping("/sys/user")
public class SysUserController extends BaseController {

    @Autowired BCryptPasswordEncoder passwordEncoder;

    @GetMapping("/info/{id}")
    @PreAuthorize("hasAuthority('sys:user:list')")
    public Result info(@PathVariable("id") Long id) {
        // 获取用户
        SysUser sysUser = sysUserService.getById(id);
        Assert.notNull(sysUser, "找不到该用户");
    
        List<SysRole> sysRoles = sysRoleService.listRoleByUserId(id);
        sysUser.setSysRoles(sysRoles);
        
        return Result.success(sysUser);
    }
    
    @GetMapping("/list")
    @PreAuthorize("hasAuthority('sys:user:list')")
    public Result list(String username) {
        
        Page<SysUser> pageData = sysUserService.page(
                getPage(),
                new QueryWrapper<SysUser>().like(StringUtils.isNotBlank(username), "username", username));
        
        // 获取角色信息
        pageData.getRecords().forEach(u -> u.setSysRoles(sysRoleService.listRoleByUserId(u.getId())));
        
        return Result.success(pageData);
    }
    
    @PostMapping("/save")
    @PreAuthorize("hasAuthority('sys:user:save')")
    public Result save(@Validated @RequestBody SysUser sysUser) {
        sysUser.setCreated(LocalDateTime.now());
        // sysUser.setUpdated(LocalDateTime.now());
        // sysUser.setLastLogin(LocalDateTime.now());
        // sysRole.setStatu(Const.STATUS_ON);
        // 默认密码，头像
        sysUser.setPassword(passwordEncoder.encode(Const.DEFULT_PASSWORD));
        sysUser.setAvatar(Const.DEFULT_AVATAR);
    
        sysUserService.save(sysUser);
        return Result.success(sysUser);
    }
    
    @PostMapping("/update")
    @PreAuthorize("hasAuthority('sys:user:update')")
    public Result update(@Validated @RequestBody SysUser sysUser) {
        sysUser.setUpdated(LocalDateTime.now());
        sysUserService.updateById(sysUser);
        
        return Result.success(sysUser);
    }
    
    @Transactional
    @PostMapping("/delete")
    @PreAuthorize("hasAuthority('sys:user:delete')")
    public Result delete(@RequestBody Long[] userIds) {
    
        sysUserService.removeByIds(Arrays.asList(userIds));
        // 删除相关的中间表
        sysUserRoleService.remove(new QueryWrapper<SysUserRole>().in("user_id", userIds));
        
        return Result.success("");
    }
    
    @Transactional
    @PostMapping("/role/{userId}")
    @PreAuthorize("hasAuthority('sys:user:role')")
    public Result rolePerm(@PathVariable("userId") Long userId, @RequestBody Long[] roleIds) {
        
        List<SysUserRole> sysUserRoles = new ArrayList<>();

        Arrays.stream(roleIds)
                .forEach(
                        r -> {
                            SysUserRole sysUserRole = new SysUserRole();
                            sysUserRole.setRoleId(r);
                            sysUserRole.setUserId(userId);

                            sysUserRoles.add(sysUserRole);
                        });

        sysUserRoleService.remove(new QueryWrapper<SysUserRole>().eq("user_id", userId));
        sysUserRoleService.saveBatch(sysUserRoles);

        // 删除缓存
        SysUser sysUser = sysUserService.getById(userId);
        sysUserService.clearUserAuthorityInfo(sysUser.getId());
        return Result.success("");
    }
    
    @PostMapping("/repass")
    @PreAuthorize("hasAuthority('sys:user:repass')")
    public Result repassword(@RequestBody Long userId) {
    
        SysUser sysUser = sysUserService.getById(userId);
    
        sysUser.setPassword(passwordEncoder.encode(Const.DEFULT_PASSWORD));
        sysUser.setUpdated(LocalDateTime.now());

        sysUserService.updateById(sysUser);

        return Result.success("");
    }
    
    @PostMapping("/updatePass")
    public Result updatePass(@RequestBody PassDto passDto, Principal principal) {

        SysUser sysUser = sysUserService.getByUsername(principal.getName());
    
        boolean matches = passwordEncoder.matches(passDto.getCurrentPassword(), sysUser.getPassword());
        if (!matches) {
            return Result.fail("旧密码不正确");
        }
    
        sysUser.setPassword(passwordEncoder.encode(passDto.getPassword()));
        sysUser.setUpdated(LocalDateTime.now());
        
        sysUserService.updateById(sysUser);
        return Result.success("");
    }
    
}
