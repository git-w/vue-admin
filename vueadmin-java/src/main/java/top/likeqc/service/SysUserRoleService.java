package top.likeqc.service;

import com.baomidou.mybatisplus.extension.service.IService;
import top.likeqc.entity.SysUserRole;

/**
 * 服务类
 *
 * @author likeqc
 * @since 2022-03-14
 */
public interface SysUserRoleService extends IService<SysUserRole> {}
