package top.likeqc.service;

import com.baomidou.mybatisplus.extension.service.IService;
import top.likeqc.entity.SysUser;

/**
 * 服务类
 *
 * @author likeqc
 * @since 2022-03-14
 */
public interface SysUserService extends IService<SysUser> {
    SysUser getByUsername(String username);
    
    String getUserAuthorityInfo(Long userId);
    
    void clearUserAuthorityInfo(Long userId);
    void clearUserAuthorityInfoByRoleId(Long roleId);
    void clearUserAuthorityInfoByMenuId(Long menuId);

}
