package top.likeqc.service.impl;

import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import top.likeqc.common.dto.SysMenuDto;
import top.likeqc.entity.SysMenu;
import top.likeqc.entity.SysUser;
import top.likeqc.mapper.SysMenuMapper;
import top.likeqc.mapper.SysUserMapper;
import top.likeqc.service.SysMenuService;
import top.likeqc.service.SysUserService;

import java.util.ArrayList;
import java.util.List;

/**
 * 服务实现类
 *
 * @author likeqc
 * @since 2022-03-14
 */
@Service
public class SysMenuServiceImpl extends ServiceImpl<SysMenuMapper, SysMenu>
        implements SysMenuService {

    @Autowired SysUserService sysUserService;

    @Autowired SysUserMapper sysUserMapper;

    @Override
    public List<SysMenuDto> getCurrentUserNav() {
        
        String username = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        SysUser sysUser = sysUserService.getByUsername(username);
    
        List<Long> menuIds = sysUserMapper.getNavMenuId(sysUser.getId());
    
        List<SysMenu> menus = this.listByIds(menuIds);
        
        // 转换数据结构
        List<SysMenu> menuTree = buildTreeMenu(menus);
        // 实体转 DTO
        
        return convert(menuTree);
    }
    
    @Override
    public List<SysMenu> tree() {
        // 获取所有菜单信息
        List<SysMenu> sysMenus = this.list(new QueryWrapper<SysMenu>().orderByAsc("orderNum"));
        // 转成树状结构
        
        return buildTreeMenu(sysMenus);
    }
    
    private List<SysMenuDto> convert(List<SysMenu> menuTree) {
        List<SysMenuDto> menuDtos = new ArrayList<>();

        menuTree.forEach(
                m -> {
                    SysMenuDto dto = new SysMenuDto();
                    dto.setId(m.getId());
                    dto.setName(m.getPerms());
                    dto.setTitle(m.getName());
                    dto.setComponent(m.getComponent());
                    dto.setPath(m.getPath());

                    if (m.getChildren().size() > 0) {
                        dto.setChildren(convert(m.getChildren()));
                    }

                    menuDtos.add(dto);
                });

        return menuDtos;
    }
    
    private List<SysMenu> buildTreeMenu(List<SysMenu> menus) {
        List<SysMenu> finalMenus = new ArrayList<>();

        // 先各自寻找到各自的孩子
        for (SysMenu menu : menus) {
            //
            for (SysMenu e : menus) {
                if (e.getParentId() == menu.getId()) {
                    menu.getChildren().add(e);
                }
            }
    
            // 提取出父节点
            if (menu.getParentId() == 0L) {
                finalMenus.add(menu);
            }
        }

        // System.out.println(JSONUtil.toJsonStr(finalMenus));

        return finalMenus;
    }
}
