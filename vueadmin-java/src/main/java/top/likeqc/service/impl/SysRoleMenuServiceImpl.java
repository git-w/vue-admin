package top.likeqc.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import top.likeqc.entity.SysRoleMenu;
import top.likeqc.mapper.SysRoleMenuMapper;
import top.likeqc.service.SysRoleMenuService;

/**
 * 服务实现类
 *
 * @author likeqc
 * @since 2022-03-14
 */
@Service
public class SysRoleMenuServiceImpl extends ServiceImpl<SysRoleMenuMapper, SysRoleMenu>
        implements SysRoleMenuService {}
