package top.likeqc.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import top.likeqc.entity.SysRole;
import top.likeqc.mapper.SysRoleMapper;
import top.likeqc.service.SysRoleService;

import java.util.List;

/**
 * 服务实现类
 *
 * @author likeqc
 * @since 2022-03-14
 */
@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole>
        implements SysRoleService {
    @Override
    public List<SysRole> listRoleByUserId(Long id) {
    
        List<SysRole> sysRoles = this.list(
                new QueryWrapper<SysRole>()
                        .inSql("id", "select role_id from sys_user_role where user_id = " + id));
        return sysRoles;
    }
}
