package top.likeqc.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import top.likeqc.entity.SysUserRole;
import top.likeqc.mapper.SysUserRoleMapper;
import top.likeqc.service.SysUserRoleService;

/**
 * 服务实现类
 *
 * @author likeqc
 * @since 2022-03-14
 */
@Service
public class SysUserRoleServiceImpl extends ServiceImpl<SysUserRoleMapper, SysUserRole>
        implements SysUserRoleService {}
