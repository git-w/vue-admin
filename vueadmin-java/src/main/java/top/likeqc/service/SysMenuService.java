package top.likeqc.service;

import com.baomidou.mybatisplus.extension.service.IService;
import top.likeqc.common.dto.SysMenuDto;
import top.likeqc.entity.SysMenu;

import java.util.List;

/**
 * 服务类
 *
 * @author likeqc
 * @since 2022-03-14
 */
public interface SysMenuService extends IService<SysMenu> {
    List<SysMenuDto> getCurrentUserNav();
    
    List<SysMenu> tree();
}
