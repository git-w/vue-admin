package top.likeqc.service;

import com.baomidou.mybatisplus.extension.service.IService;
import top.likeqc.entity.SysRole;

import java.util.List;

/**
 * 服务类
 *
 * @author likeqc
 * @since 2022-03-14
 */
public interface SysRoleService extends IService<SysRole> {
    List<SysRole> listRoleByUserId(Long id);
}
