package top.likeqc.service;

import com.baomidou.mybatisplus.extension.service.IService;
import top.likeqc.entity.SysRoleMenu;

/**
 * 服务类
 *
 * @author likeqc
 * @since 2022-03-14
 */
public interface SysRoleMenuService extends IService<SysRoleMenu> {}
